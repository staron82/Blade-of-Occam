using UnityEngine;
using UnityEngine.InputSystem;
using Lightbug.CharacterControllerPro.Implementation;

public class NewInputSystemHandler : InputHandler
{
    [SerializeField]
    InputActionAsset characterActionsAsset = null;

    void Awake()
    {
        characterActionsAsset.Enable();
    }

    public override float GetFloat(string actionName)
    {
        return characterActionsAsset.FindAction(actionName).ReadValue<float>();
    }

    public override bool GetBool(string actionName)
    {
        return characterActionsAsset.FindAction(actionName).ReadValue<float>() >= InputSystem.settings.defaultButtonPressPoint;
    }

    public override Vector2 GetVector2(string actionName)
    {
        return characterActionsAsset.FindAction(actionName).ReadValue<Vector2>();
    }
}