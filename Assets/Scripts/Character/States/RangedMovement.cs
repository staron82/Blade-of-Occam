using Lightbug.CharacterControllerPro.Implementation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedMovement : CharacterState
{
    [SerializeField] private IKController ik;
    [SerializeField] private Transform targetTransform;
    [Tooltip("Layers to be considered as ground (walkables). Used by ground click detection.")]
    [SerializeField]
    private LayerMask groundMask = 1;            // Default layer

    public override void EnterBehaviour(float dt, CharacterState fromState)
    {
        base.EnterBehaviour(dt, fromState);
    }

    public override void UpdateBehaviour(float dt)
    {
        CalculateAim(dt);
        ik.UpdateIK();
    }

    private void CalculateAim(float dt)
    {
        var ray = Camera.main.ScreenPointToRay(CharacterActions.aim.value);

        RaycastHit hitInfo;
        if (!Physics.Raycast(ray, out hitInfo, Mathf.Infinity, groundMask))
            return;

        targetTransform.position = hitInfo.point;
    }

    public override void PostUpdateBehaviour(float dt)
    {
        base.PostUpdateBehaviour(dt);
    }

    private void LateUpdate()
    {
        
    }

    public override void UpdateIK(int layerIndex)
    {
        //base.UpdateIK(layerIndex);
        Debug.Log(2);
    }
}
