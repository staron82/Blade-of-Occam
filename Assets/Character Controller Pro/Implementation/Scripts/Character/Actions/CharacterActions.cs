namespace Lightbug.CharacterControllerPro.Implementation
{

/// <summary>
/// This struct contains all the inputs actions available for the character to interact with.
/// </summary>
[System.Serializable]
public struct CharacterActions 
{
    
    // Bool actions


    // Float actions


    // Vector2 actions
	public Vector2Action @aim;
	public Vector2Action @movement;


     
    /// <summary>
    /// Reset all the actions.
    /// </summary>
	public void Reset()
	{


		@aim.Reset();
		@movement.Reset();

	}

    /// <summary>
    /// Initializes all the actions by instantiate them. Each action will be instantiated with its specific type (Bool, Float or Vector2).
    /// </summary>
    public void InitializeActions()
    {


		@aim = new Vector2Action();
		@movement = new Vector2Action();

    }

    /// <summary>
    /// Updates the values of all the actions based on the current input handler (human).
    /// </summary>
    public void SetValues( InputHandler inputHandler )
    {
        if( inputHandler == null )
			return;
        


		@aim.value = inputHandler.GetVector2( "Aim" );
		@movement.value = inputHandler.GetVector2( "Movement" );

    }

    /// <summary>
    /// Copies the values of all the actions from an existing set of actions.
    /// </summary>
    public void SetValues( CharacterActions characterActions )
    {	


		@aim.value = characterActions.aim.value;
		@movement.value = characterActions.movement.value;

    }

    /// <summary>
	/// Update all the actions internal states.
	/// </summary>
    public void Update( float dt )
    {

    }


}


}